package simulation;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import simulation.model.WorldSimulation;

public class Main {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("World Simulation");
        cfg.setWindowedMode(1200, 800);

        new Lwjgl3Application(new WorldSimulation(), cfg);
    }
}
