package simulation.model;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;

import simulation.view.View;

public class WorldSimulation extends Game {
    private World world;
    private Vector2 playerGripPosition = new Vector2(3, -6);
    private Body playerBody;
    public static final float weaponWidth = (float) (18*Math.sqrt(2));
    public static final float weaponHeight = 6;
    public static final float playerWidth = 30;
    public static final float playerHeight = 52;
    private Body weaponBody;
    public static final float DEGREE_TO_RADIAN = (float)(Math.PI/180);

    //Fixture bits
    public static final short DEFAULT = 1;
    public static final short PLAYER_BIT = 2;
    public static final short WEAPON_BIT = 4;
    public static final short MAP_OBJECT_BIT = 8;
    public static final short ENEMY_BIT = 16;



    @Override
    public void create() {
        world = new World(new Vector2(0.0f, 0.0f), false);

        playerBody = createPlayer();

        Vector2 weaponOrigin = new Vector2(
                weaponWidth / 2f /View.PPM,
                0);
        weaponBody = createWeapon(weaponOrigin);

        createDistanceJoint(playerBody, weaponBody);

        setScreen(new View(this));

        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.A) {
                    weaponBody.setAngularVelocity(-5);
                    System.out.println("works");
                }

                if (keycode == Input.Keys.SPACE) {
                    world.step(Gdx.graphics.getDeltaTime(), 6, 2);
                }
                return true;
            }
        });

        System.out.println(world.getBodyCount());
    }

    private Body createPlayer() {
        Body pBody;
        BodyDef bodyDef = getBodyDef(0, 0, BodyType.DynamicBody);
        bodyDef.fixedRotation = true;

        pBody = world.createBody(bodyDef);

        FixtureDef fDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(playerWidth / 2f / View.PPM, playerHeight / 2f / View.PPM);
        fDef.shape = shape;
        fDef.filter.categoryBits = PLAYER_BIT;
        fDef.filter.maskBits = DEFAULT | MAP_OBJECT_BIT | ENEMY_BIT;
        pBody.createFixture(fDef);
        shape.dispose();

        return pBody;
    }

    private Body createWeapon(Vector2 origin) {
        Body pBody;
        BodyDef bodyDef = getBodyDef(
                playerGripPosition.x,
                playerGripPosition.y,
                BodyType.DynamicBody);
        bodyDef.fixedRotation = false;

        pBody = world.createBody(bodyDef);

        FixtureDef fDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(
                weaponWidth / 2f / View.PPM,
                weaponHeight / 2f / View.PPM,
                origin, 0);
        pBody.setTransform(pBody.getWorldCenter(), 45*DEGREE_TO_RADIAN);
        fDef.shape = shape;
        fDef.filter.categoryBits = WEAPON_BIT;
        fDef.filter.maskBits = DEFAULT | MAP_OBJECT_BIT | ENEMY_BIT;
        pBody.createFixture(fDef);
        shape.dispose();

        return pBody;
    }

    public World getWorld() {
        return this.world;
    }

    public Body getPlayer() {
        return this.playerBody;
    }

    public Body getWeapon() {
        return this.weaponBody;
    }

    /** Inspired by: <a href="https://lyze.dev/2021/03/25/libGDX-Tiled-Box2D-example/">...</a> * */
    private BodyDef getBodyDef(float x, float y, BodyType bodyType) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;

        System.out.println(bodyDef.type);

        bodyDef.position.set(x / View.PPM, y / View.PPM);
    
        return bodyDef;
    }

    private void createDistanceJoint(Body bodyA, Body bodyB) {
        DistanceJointDef jointDef = new DistanceJointDef();
        jointDef.initialize(bodyA, bodyB, bodyA.getWorldCenter(), bodyB.getWorldCenter());
        jointDef.collideConnected = false;
        jointDef.length = (float) Math.abs(Math.sqrt(Math.pow(bodyB.getPosition().x - bodyA.getPosition().x, 2) + Math.pow(bodyB.getPosition().y - bodyA.getPosition().y, 2)));
        world.createJoint(jointDef);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        // System.out.println(playerGrip.getAngle());
        playerBody.setLinearVelocity(Vector2.Zero);
        if (weaponBody.getAngle() <= -45*DEGREE_TO_RADIAN) {
            weaponBody.setAngularVelocity(0);
            weaponBody.setTransform(weaponBody.getWorldCenter(), 45*DEGREE_TO_RADIAN);
        }
        world.step(Gdx.graphics.getDeltaTime(), 6, 2);
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
        world.dispose();
    }

}
