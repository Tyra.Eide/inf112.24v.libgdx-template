package simulation.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import simulation.model.WorldSimulation;

public class View extends ScreenAdapter {
    public static final float PPM = 24;
    private WorldSimulation model;
    private Box2DDebugRenderer b2dr;
    private OrthographicCamera camera;
    private Batch spriteBatch;
    private ShapeRenderer shapeRenderer;

    private Texture playerSprite = new Texture(Gdx.files.internal("AttackRight-1.png"));
    private Texture weaponTexture = new Texture(Gdx.files.internal("SwordAttackRight-1.png"));

    private Sprite weaponSprite = new Sprite(weaponTexture);
    
    public View(WorldSimulation model) {
        this.model = model;
        this.b2dr = new Box2DDebugRenderer();
        this.camera = cameraSetup();
        this.spriteBatch = new SpriteBatch();
        this.shapeRenderer = new ShapeRenderer();

        weaponSprite.setOrigin(
                0,0
        );
        
    }

    private OrthographicCamera cameraSetup() {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);
        camera.zoom = .2f;
        camera.update();
        return camera;
    }
    
      /**
   * Source: <a href=
   * https://www.youtube.com/watch?v=_y1RvNWoRFU&list=PLD_bW3UTVsElsuvyKcYXHLnWb8bD0EQNI></a>*
   */
    protected void updateCamera() {
    Vector3 position = camera.position;
    position.x = model.getPlayer().getPosition().x * PPM;
    position.y = model.getPlayer().getPosition().y * PPM;
    camera.update();
  }


    @Override
    public void render(float v) {
        Gdx.gl.glClearColor(0, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        updateCamera();
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();
        spriteBatch.draw(
            playerSprite,
            model.getPlayer().getPosition().x * PPM - WorldSimulation.playerWidth / 2f, 
            model.getPlayer().getPosition().y * PPM - WorldSimulation.playerHeight / 2f
            );

        weaponSprite.setPosition(
            model.getWeapon().getPosition().x * PPM,
            model.getWeapon().getPosition().y * PPM
        );

        weaponSprite.setRotation(model.getWeapon().getAngle() / WorldSimulation.DEGREE_TO_RADIAN - 45);
        // System.out.println(model.getWeapon().getAngle()*PPM / WorldSimulation.DEGREE_TO_RADIAN);
        weaponSprite.draw(spriteBatch);
        spriteBatch.end();

        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.setColor(0, 0, 0, 1);
        shapeRenderer.rect(model.getWeapon().getPosition().x, model.getWeapon().getPosition().y, WorldSimulation.weaponWidth, WorldSimulation.weaponHeight);
        shapeRenderer.end();

        b2dr.render(model.getWorld(), camera.combined.scl(PPM));
    }

    @Override
    public void dispose() {
        b2dr.dispose();
        spriteBatch.dispose();
        shapeRenderer.dispose();
    }


}
